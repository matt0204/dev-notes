# dev-notes

**ANDROID**
*  ./gradlew assembleDebug --info   // run gradle compilation in console
*  Speep up gradle :
https://medium.com/mindorks/lets-speed-up-your-gradle-build-6c0e401883e2

In gradle.properties :
`org.gradle.parallel=true
org.gradle.caching= true`

In build.gradle (app) :
`    if(project.hasProperty('devBuild')) {
        splits.abi.enable = false
        splits.density.enable = false
    }` and file -> settings -> Command-line Options add -Pdevbuild
